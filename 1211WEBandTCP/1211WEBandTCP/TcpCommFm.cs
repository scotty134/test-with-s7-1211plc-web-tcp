﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using _1211WEBandTCP.tcp;
using System.Net;

namespace _1211WEBandTCP
{
    public partial class TcpCommFm : Form
    {
        ServerTCP tcpReader;
        ClientTCP tcpWriter;

        bool readStatus;
        bool writeStatus;

        public TcpCommFm()
        {
            InitializeComponent();
            readStatus = false;
            writeStatus = false;
        }

        private void txtPort_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private async void btnConnect_Click(object sender, EventArgs e)
        {
            try
            {
                Int16 portMy = Convert.ToInt16(txtMyPort.Text);
                IPAddress ipAdrsMy = IPAddress.Parse(txtMyIP.Text);
                //IPAddress ipAdrsClient = IPAddress.Parse(txtClientIP.Text);
                //Int16 portClient = Convert.ToInt16(txtClientPort.Text);

                tcpReader = new ServerTCP(portMy, ipAdrsMy);
                tcpReader.Start();
                //вывести сообщение
                txtBuffer.Text += tcpReader.message;
                this.Update();
                await Task.Run(() => tcpReader.AcceptListener());
                //коммуникация good                
                txtBuffer.Text += "\r\nCommunication connected for writer.";
                this.Update();

                timerPLC.Interval = Convert.ToInt16(txtTimer.Text);
                Thread.Sleep(500);

                readStatus = true;
                if (!timerPLC.Enabled)
                    timerPLC.Start();
                StatusLabel.BackColor = Color.Green;
            }
            catch (Exception ex)
            {
                txtBuffer.Text += ex.Message;
            }
            this.Update();
        }

        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            //timerPLC.Stop();
            if (readStatus)
            {
                readStatus = false;

                Thread.Sleep(1500);
                if (tcpReader.IsConnected())
                    tcpReader.Stop();
                StatusLabel.BackColor = Color.Red;
                txtBuffer.Text += "\r\nReader disconnected";
                this.Update();
            }
        }

        private void timerPLC_Tick(object sender, EventArgs e)
        {
            if (readStatus)
            {
                if (tcpReader.IsConnected())
                {
                    ReadData();
                }
            }
        }

        private void TcpCommFm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (timerPLC.Enabled)
            {
                timerPLC.Stop();
            }
            if (readStatus)
            {             
                if (tcpReader.IsConnected())
                    tcpReader.Stop();
            }
            if (writeStatus)
            {
                if (tcpWriter.IsConnected())
                    tcpWriter.Stop();               
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void ReadData()
        {
            // read 
            byte[] bytes = new byte[256];
            string outLine = String.Empty;
            bool status = tcpReader.Read(out bytes, out outLine);
            if (!status)
                txtBuffer.Text += "\r\nCannot Read!";

            txtRecieveBytes.Text = BitConverter.ToString(bytes);
            txtRecieveText.Text = outLine;
            //numRcv.Value = bytes.Length;
            numRcv.Value = outLine.Length;
            this.Update();
        }

        /// <summary>
        /// 
        /// </summary>
        private void WriteData()
        {
            // write for Reader
            string inLine = txtSendText.Text;
            numSend.Value = inLine.Length;
            this.Update();
            byte[] data = new byte[256];
            data = System.Text.Encoding.ASCII.GetBytes(inLine);
            txtSendBytes.Text = BitConverter.ToString(data);
            this.Update();
            //tcpReader.Write(inLine);            
        }

        /// <summary>
        /// 
        /// </summary>
        private void WriteDataW()
        {
            // write for Writer            
            string inLine = txtSendText.Text;// +"\r\n";
            numSend.Value = inLine.Length;
            this.Update();
            byte[] data = new byte[256];
            data = System.Text.Encoding.ASCII.GetBytes(inLine);
            txtSendBytes.Text = BitConverter.ToString(data);
            this.Update();
            //tcpWriter.Write(inLine);
            tcpWriter.BeginSend(inLine);
        }

        /// <summary>
        /// 
        /// </summary>
        private void SendPacket()
        {
            try
            {
                string inLine = txtSendText.Text;
                numSend.Value = inLine.Length;
                this.Update();
                byte[] data = new byte[256];
                data = System.Text.Encoding.ASCII.GetBytes(inLine);
                txtSendBytes.Text = BitConverter.ToString(data);
                this.Update();
                tcpWriter.SendPacket(inLine);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void btnWriteDisconn_Click(object sender, EventArgs e)
        {
            if (writeStatus)
            {
                writeStatus = false;

                Thread.Sleep(1500);
                if (tcpWriter.IsConnected())
                    tcpWriter.Stop();
                statusWrite.BackColor = Color.Red;
                txtBuffer.Text += "\r\nDisconnected";
                this.Update();
            }
        }

        private async void btnWriteConn_Click(object sender, EventArgs e)
        {
            try
            {
                Int16 portMy = Convert.ToInt16(txtMyPort.Text);
                IPAddress ipAdrsMy = IPAddress.Parse(txtMyIP.Text);

                IPAddress ipAdrsClient = IPAddress.Parse(txtClientIP.Text);
                Int16 portClient = Convert.ToInt16(txtClientPort.Text);

                IPEndPoint epServ = new IPEndPoint(ipAdrsMy, portClient);
                IPEndPoint epClient = new IPEndPoint(ipAdrsClient, portClient);

                tcpWriter = new ClientTCP(portClient, ipAdrsMy);
                tcpWriter.Start();
                //вывести сообщение
                txtBuffer.Text += tcpWriter.message;
                this.Update();
                //tcpWriter.SocketInit(epServ, epClient);
                await Task.Run(() => tcpWriter.AcceptListener());
                
                //коммуникация good                
                txtBuffer.Text += "\r\nCommunication connected for writer.";
                this.Update();

                timerPLC.Interval = Convert.ToInt16(txtTimer.Text);
                Thread.Sleep(500);

                writeStatus = true;
                if(!timerPLC.Enabled)
                    timerPLC.Start();
                statusWrite.BackColor = Color.Green;
            }
            catch (Exception ex)
            {
                txtBuffer.Text += ex.Message;
            }
            this.Update();
        }


        private void btnSend_Click(object sender, EventArgs e)
        {
            // for send 10 symbols need format like this = ++S111222F
            if (writeStatus)
            {
                if (tcpWriter.IsConnected())
                {
                    WriteDataW();
                }
            }
        }
    }
}
