﻿namespace _1211WEBandTCP
{
    partial class WebCommFm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtURL = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblBool = new System.Windows.Forms.Label();
            this.txtTimer = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.lblInt = new System.Windows.Forms.Label();
            this.lblDInt = new System.Windows.Forms.Label();
            this.lblReal = new System.Windows.Forms.Label();
            this.lblString = new System.Windows.Forms.Label();
            this.timerUpload = new System.Windows.Forms.Timer(this.components);
            this.btnReading = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtInt = new System.Windows.Forms.TextBox();
            this.txtDInt = new System.Windows.Forms.TextBox();
            this.txtReal = new System.Windows.Forms.TextBox();
            this.txtString = new System.Windows.Forms.TextBox();
            this.cmbBool = new System.Windows.Forms.ComboBox();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.label16 = new System.Windows.Forms.Label();
            this.txtIP = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtXML = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtURL
            // 
            this.txtURL.Location = new System.Drawing.Point(52, 12);
            this.txtURL.Name = "txtURL";
            this.txtURL.Size = new System.Drawing.Size(381, 20);
            this.txtURL.TabIndex = 0;
            this.txtURL.Text = "http://172.20.5.200/awp/testPage/newPage.html";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(11, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "URL :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(79, 143);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Get :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(335, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Set :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label4.Location = new System.Drawing.Point(8, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(221, 17);
            this.label4.TabIndex = 18;
            this.label4.Text = "Интервал опроса контроллера :";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSubmit.Location = new System.Drawing.Point(338, 342);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(75, 23);
            this.btnSubmit.TabIndex = 19;
            this.btnSubmit.Text = "submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(24, 195);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 17);
            this.label5.TabIndex = 20;
            this.label5.Text = "Bool \"b1\" :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(24, 223);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 17);
            this.label6.TabIndex = 21;
            this.label6.Text = "Int \"i1\" :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(24, 249);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 17);
            this.label7.TabIndex = 22;
            this.label7.Text = "DInt \"di1\" :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(24, 273);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 17);
            this.label8.TabIndex = 23;
            this.label8.Text = "Real \"r1\" :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(24, 301);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 17);
            this.label9.TabIndex = 24;
            this.label9.Text = "String \"s1\" :";
            // 
            // lblBool
            // 
            this.lblBool.AutoSize = true;
            this.lblBool.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblBool.Location = new System.Drawing.Point(114, 195);
            this.lblBool.Name = "lblBool";
            this.lblBool.Size = new System.Drawing.Size(35, 15);
            this.lblBool.TabIndex = 25;
            this.lblBool.Text = "лддл";
            this.lblBool.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTimer
            // 
            this.txtTimer.Location = new System.Drawing.Point(235, 87);
            this.txtTimer.Name = "txtTimer";
            this.txtTimer.Size = new System.Drawing.Size(58, 20);
            this.txtTimer.TabIndex = 26;
            this.txtTimer.Text = "1000";
            this.txtTimer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(299, 90);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 13);
            this.label11.TabIndex = 27;
            this.label11.Text = "мс";
            // 
            // lblInt
            // 
            this.lblInt.AutoSize = true;
            this.lblInt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblInt.Location = new System.Drawing.Point(114, 223);
            this.lblInt.Name = "lblInt";
            this.lblInt.Size = new System.Drawing.Size(35, 15);
            this.lblInt.TabIndex = 28;
            this.lblInt.Text = "лддл";
            this.lblInt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDInt
            // 
            this.lblDInt.AutoSize = true;
            this.lblDInt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblDInt.Location = new System.Drawing.Point(115, 249);
            this.lblDInt.Name = "lblDInt";
            this.lblDInt.Size = new System.Drawing.Size(35, 15);
            this.lblDInt.TabIndex = 29;
            this.lblDInt.Text = "лддл";
            this.lblDInt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblReal
            // 
            this.lblReal.AutoSize = true;
            this.lblReal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblReal.Location = new System.Drawing.Point(114, 275);
            this.lblReal.Name = "lblReal";
            this.lblReal.Size = new System.Drawing.Size(35, 15);
            this.lblReal.TabIndex = 30;
            this.lblReal.Text = "лддл";
            this.lblReal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblString
            // 
            this.lblString.AutoSize = true;
            this.lblString.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblString.Location = new System.Drawing.Point(114, 301);
            this.lblString.Name = "lblString";
            this.lblString.Size = new System.Drawing.Size(35, 15);
            this.lblString.TabIndex = 31;
            this.lblString.Text = "лддл";
            this.lblString.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // timerUpload
            // 
            this.timerUpload.Tick += new System.EventHandler(this.timerUpload_Tick);
            // 
            // btnReading
            // 
            this.btnReading.Location = new System.Drawing.Point(9, 108);
            this.btnReading.Name = "btnReading";
            this.btnReading.Size = new System.Drawing.Size(69, 35);
            this.btnReading.TabIndex = 32;
            this.btnReading.Text = "Start Reading";
            this.btnReading.UseVisualStyleBackColor = true;
            this.btnReading.Click += new System.EventHandler(this.btnReading_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(260, 301);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 17);
            this.label10.TabIndex = 37;
            this.label10.Text = "String \"s1\" :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(260, 275);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 17);
            this.label12.TabIndex = 36;
            this.label12.Text = "Real \"r1\" :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(260, 249);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(74, 17);
            this.label13.TabIndex = 35;
            this.label13.Text = "DInt \"di1\" :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(260, 223);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 17);
            this.label14.TabIndex = 34;
            this.label14.Text = "Int \"i1\" :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(260, 195);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(74, 17);
            this.label15.TabIndex = 33;
            this.label15.Text = "Bool \"b1\" :";
            // 
            // txtInt
            // 
            this.txtInt.Location = new System.Drawing.Point(350, 222);
            this.txtInt.Name = "txtInt";
            this.txtInt.Size = new System.Drawing.Size(100, 20);
            this.txtInt.TabIndex = 38;
            this.txtInt.Text = "1375";
            this.txtInt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtDInt
            // 
            this.txtDInt.Location = new System.Drawing.Point(350, 248);
            this.txtDInt.Name = "txtDInt";
            this.txtDInt.Size = new System.Drawing.Size(100, 20);
            this.txtDInt.TabIndex = 39;
            this.txtDInt.Text = "133132123";
            this.txtDInt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtReal
            // 
            this.txtReal.Location = new System.Drawing.Point(350, 274);
            this.txtReal.Name = "txtReal";
            this.txtReal.Size = new System.Drawing.Size(100, 20);
            this.txtReal.TabIndex = 40;
            this.txtReal.Text = "12.11";
            this.txtReal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtString
            // 
            this.txtString.Location = new System.Drawing.Point(350, 300);
            this.txtString.Name = "txtString";
            this.txtString.Size = new System.Drawing.Size(100, 20);
            this.txtString.TabIndex = 41;
            this.txtString.Text = "sasai";
            this.txtString.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cmbBool
            // 
            this.cmbBool.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBool.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmbBool.FormattingEnabled = true;
            this.cmbBool.Items.AddRange(new object[] {
            "ON",
            "OFF"});
            this.cmbBool.Location = new System.Drawing.Point(350, 194);
            this.cmbBool.Name = "cmbBool";
            this.cmbBool.Size = new System.Drawing.Size(63, 21);
            this.cmbBool.TabIndex = 42;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(459, 1);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(335, 366);
            this.webBrowser1.TabIndex = 43;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(25, 65);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(28, 17);
            this.label16.TabIndex = 45;
            this.label16.Text = "IP :";
            // 
            // txtIP
            // 
            this.txtIP.Location = new System.Drawing.Point(50, 63);
            this.txtIP.Name = "txtIP";
            this.txtIP.Size = new System.Drawing.Size(99, 20);
            this.txtIP.TabIndex = 44;
            this.txtIP.Text = "172.20.5.200";
            this.txtIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(11, 39);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(44, 17);
            this.label17.TabIndex = 47;
            this.label17.Text = "XML :";
            // 
            // txtXML
            // 
            this.txtXML.Location = new System.Drawing.Point(52, 37);
            this.txtXML.Name = "txtXML";
            this.txtXML.Size = new System.Drawing.Size(381, 20);
            this.txtXML.TabIndex = 46;
            this.txtXML.Text = "http://172.20.5.200/awp/testPage/dataFile.xml";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 368);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(794, 24);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 48;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(20, 19);
            this.StatusLabel.Text = "   ";
            // 
            // WebCommFm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 392);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtXML);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtIP);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.cmbBool);
            this.Controls.Add(this.txtString);
            this.Controls.Add(this.txtReal);
            this.Controls.Add(this.txtDInt);
            this.Controls.Add(this.txtInt);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.btnReading);
            this.Controls.Add(this.lblString);
            this.Controls.Add(this.lblReal);
            this.Controls.Add(this.lblDInt);
            this.Controls.Add(this.lblInt);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtTimer);
            this.Controls.Add(this.lblBool);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtURL);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "WebCommFm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Communication with WEB server of PLC";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WebCommFm_FormClosing);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtURL;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblBool;
        private System.Windows.Forms.TextBox txtTimer;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblInt;
        private System.Windows.Forms.Label lblDInt;
        private System.Windows.Forms.Label lblReal;
        private System.Windows.Forms.Label lblString;
        private System.Windows.Forms.Timer timerUpload;
        private System.Windows.Forms.Button btnReading;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtInt;
        private System.Windows.Forms.TextBox txtDInt;
        private System.Windows.Forms.TextBox txtReal;
        private System.Windows.Forms.TextBox txtString;
        private System.Windows.Forms.ComboBox cmbBool;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtIP;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtXML;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel;
    }
}