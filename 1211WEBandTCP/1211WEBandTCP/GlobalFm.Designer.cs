﻿namespace _1211WEBandTCP
{
    partial class GlobalFm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTCP = new System.Windows.Forms.Button();
            this.btnWEB = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnTCP
            // 
            this.btnTCP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTCP.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnTCP.Location = new System.Drawing.Point(38, 29);
            this.btnTCP.Name = "btnTCP";
            this.btnTCP.Size = new System.Drawing.Size(166, 61);
            this.btnTCP.TabIndex = 0;
            this.btnTCP.Text = "TCP/IP communicate";
            this.btnTCP.UseVisualStyleBackColor = true;
            this.btnTCP.Click += new System.EventHandler(this.btnTCP_Click);
            // 
            // btnWEB
            // 
            this.btnWEB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnWEB.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnWEB.Location = new System.Drawing.Point(37, 103);
            this.btnWEB.Name = "btnWEB";
            this.btnWEB.Size = new System.Drawing.Size(166, 61);
            this.btnWEB.TabIndex = 1;
            this.btnWEB.Text = "working with WEB server";
            this.btnWEB.UseVisualStyleBackColor = true;
            this.btnWEB.Click += new System.EventHandler(this.btnWEB_Click);
            // 
            // GlobalFm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(240, 209);
            this.Controls.Add(this.btnWEB);
            this.Controls.Add(this.btnTCP);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GlobalFm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Global screen";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnTCP;
        private System.Windows.Forms.Button btnWEB;
    }
}

