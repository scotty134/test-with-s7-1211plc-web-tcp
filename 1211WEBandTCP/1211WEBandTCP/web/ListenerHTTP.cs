﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Windows.Forms;
using System.Xml.Linq;
using _1211WEBandTCP.models;

namespace _1211WEBandTCP.web
{
    public class ListenerHTTP
    {
        private HttpClient client = new HttpClient();

        public async Task<List<DataModel>> StartListen(string uri)
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(uri);

                response.EnsureSuccessStatusCode();
                string result = await response.Content.ReadAsStringAsync();

                return GetFileInfoData(result);
            }
            catch (HttpRequestException)
            {                
                return new List<DataModel>();
                throw;
            }
        }

        private List<DataModel> GetFileInfoData(string data)
        {
            data.TrimStart('\n');
            var resultData = data.Trim();
            int index = resultData.IndexOf("<dataStorage>");
            resultData = resultData.Remove(0, index);
            resultData = resultData.Insert(0, "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n");
            XDocument doc = XDocument.Parse(resultData);




            var result = (from cell in doc.Descendants("cell")
                          select new DataModel
                          {
                              boolValue = (cell.Element("boolValue").Value == "1")? true:  false,
                              intValue = Convert.ToInt32(cell.Element("intValue").Value),
                              dintValue = Convert.ToDouble(cell.Element("dintValue").Value),
                              floatValue = float.Parse(cell.Element("realValue").Value),
                              stringValue = cell.Element("stringValue").Value
                          }).ToList();

            return result;
        }
    }
}
