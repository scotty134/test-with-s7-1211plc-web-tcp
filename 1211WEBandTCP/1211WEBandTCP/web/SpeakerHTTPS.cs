﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.Net;

namespace _1211WEBandTCP.web
{
    public class SpeakerHTTPS
    {
        //Add to form with thiы class
        //ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
        //***************************************************************************

        private string cookieWithoutPath = String.Empty;
        private string cookieOnly = String.Empty;
        private WebClient wb;
        private string url;
        private string ip;

        public string getCookieWithPath() 
        {
            return this.cookieWithoutPath;          
        }

        public string getCookieOnly()
        {
            return this.cookieOnly;
        }

        private bool isOpenConnection = false;

        public SpeakerHTTPS(string url, string ipAdress)
        {
            this.url = url;
            this.ip = ipAdress;
            this.wb = new WebClient();
        }

        public SpeakerHTTPS(string ipAdress)
        {
            this.ip = ipAdress;
            this.wb = new WebClient();
        }

        public void SetDataParam()
        { 

        }

        public async Task<bool> Connection()
        {
            try
            {
                //LOGIN
                Uri uriLogin = new Uri("https://" + this.ip + "/FormLogin");
                var data = new NameValueCollection();
                data["Login"] = "admin";
                data["Password"] = "";
                await Task.Run(() => wb.UploadValues(uriLogin, "POST", data));
                int index = wb.ResponseHeaders[2].ToString().IndexOf(';');
                int indexSecond = wb.ResponseHeaders[2].ToString().IndexOf('=');
                cookieWithoutPath = wb.ResponseHeaders[2].ToString().Remove(index);
                cookieOnly = cookieWithoutPath.Remove(0, indexSecond + 1);
                isOpenConnection = true;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void Disconnect()
        {
            if (isOpenConnection)
            {
                //LOGOUT
                Uri uriLogin1 = new Uri("https://" + this.ip + "/FormLogin?LOGOUT");
                var data1 = new NameValueCollection();
                data1["Redirection"] = ".";
                data1["Cookie"] = cookieOnly;
                var response1 = wb.UploadValues(uriLogin1, "POST", data1);
                isOpenConnection = false;
            }
        }
    }
}
