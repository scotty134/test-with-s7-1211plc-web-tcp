﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Runtime.InteropServices;
using _1211WEBandTCP.web;
using _1211WEBandTCP.models;

namespace _1211WEBandTCP
{
    public partial class WebCommFm : Form
    {
        [DllImport("wininet.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern bool InternetSetCookie(string lpszUrlName, string lpszCookieName, string lpszCookieData);


        bool btnStatus = false;
        ListenerHTTP listener = new ListenerHTTP();
        SpeakerHTTPS speaker;
        bool statusHTTPS;

        public WebCommFm()
        {
            InitializeComponent();
            ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
            speaker = new SpeakerHTTPS(txtIP.Text);
            LoadingHttps();
            cmbBool.SelectedIndex = 0;
        }

        private async void LoadingHttps()
        {
            statusHTTPS = await Task.Run(() => speaker.Connection());
            if (statusHTTPS)
            {
                StatusLabel.BackColor = Color.Green;
            }
            else
            {
                StatusLabel.BackColor = Color.Red;
            }
        }

        private void btnReading_Click(object sender, EventArgs e)
        {
            timerUpload.Interval = Int32.Parse(txtTimer.Text);
            if (btnStatus)
            {
                btnStatus = false;
                timerUpload.Stop();
                btnReading.Text = "stoped!";
                btnReading.BackColor = Color.Red;
            }
            else
            {
                btnStatus = true;
                timerUpload.Start();
                btnReading.Text = "started!";
                btnReading.BackColor = Color.Green;
            }
        }

        private async void timerUpload_Tick(object sender, EventArgs e)
        {
            try
            {
                List<DataModel> dm = await Task.Run(() => listener.StartListen(txtXML.Text));
                if (dm.Count != 0)
                {
                    lblBool.Text = dm[0].boolValue.ToString();
                    lblInt.Text = dm[0].intValue.ToString();
                    lblDInt.Text = dm[0].dintValue.ToString();
                    lblReal.Text = dm[0].floatValue.ToString();
                    lblString.Text = dm[0].stringValue;
                    this.Update();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void WebCommFm_FormClosing(object sender, FormClosingEventArgs e)
        {
            timerUpload.Stop();
            speaker.Disconnect();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            DataModel dm = new DataModel();
            //bool
            switch (cmbBool.SelectedIndex)
            {
                case 0:
                    dm.boolValue = true;
                    break;
                case 1:
                    dm.boolValue = false;
                    break;
                default:
                    break;
            }
            //int
            dm.intValue = Convert.ToInt32(txtInt.Text);
            //dint
            dm.dintValue = Convert.ToDouble(txtDInt.Text);
            //float
            dm.floatValue = float.Parse(txtReal.Text);
            //string
            dm.stringValue = "'" + txtString.Text + "'";

            InternetSetCookie(txtURL.Text, null, speaker.getCookieWithPath());
            webBrowser1.Navigate(txtURL.Text);      

            webBrowser1.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(webBrowser1_DocumentCompleted);
            while (webBrowser1.ReadyState != WebBrowserReadyState.Complete)
            {
                Application.DoEvents();
            } 
            //set datas
            webBrowser1.Document.GetElementById("setBool").SetAttribute("selectedIndex", ((dm.boolValue == true)?1:2).ToString());
            webBrowser1.Document.GetElementById("setInt").SetAttribute("value", dm.intValue.ToString());
            webBrowser1.Document.GetElementById("setDInt").SetAttribute("value", dm.dintValue.ToString());
            webBrowser1.Document.GetElementById("setReal").SetAttribute("value", dm.floatValue.ToString());
            webBrowser1.Document.GetElementById("setString").SetAttribute("value", dm.stringValue);

            webBrowser1.Document.GetElementById("btnSubmit").InvokeMember("click");

        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        { 
        }
    }
}
