﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1211WEBandTCP
{
    public partial class GlobalFm : Form
    {
        public GlobalFm()
        {
            InitializeComponent();
        }

        private void btnTCP_Click(object sender, EventArgs e)
        {
            new TcpCommFm().ShowDialog();
        }

        private void btnWEB_Click(object sender, EventArgs e)
        {
            new WebCommFm().ShowDialog();
        }
    }
}
