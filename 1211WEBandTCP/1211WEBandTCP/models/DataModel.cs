﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1211WEBandTCP.models
{
    public class DataModel
    {
        public bool boolValue { get; set; }
        public int intValue { get; set; }
        public double dintValue { get; set; }
        public float floatValue { get; set; }
        public string stringValue { get; set; }
    }
}
