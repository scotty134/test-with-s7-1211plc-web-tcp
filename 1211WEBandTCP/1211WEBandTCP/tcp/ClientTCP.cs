﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace _1211WEBandTCP.tcp
{
    public class ClientTCP
    {
        private Int16 port;
        private IPAddress ipAdrs;
        private TcpListener listener;
        private TcpClient client; 
        private NetworkStream stream;
        Socket sck;

        public string message { get; set; }

        public byte[] outByte { get; set; }
        public string outString { get; set; }
        public byte[] inByte { get; set; }
        public string inString { get; set; }

        public ClientTCP(Int16 p, IPAddress ip)
        {
            this.ipAdrs = ip;
            this.port = p;            
            this.listener = new TcpListener(this.ipAdrs, this.port);
            this.client = new TcpClient();
            
            //sck = new Socket(ip.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            //sck.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
        }

        public void SocketInit(IPEndPoint serv, IPEndPoint client)
        {
            sck.Bind(serv);
            sck.Connect(client);
        }

        public void Start()
        {
            this.listener.Start();
            this.message = String.Empty;
            this.message = String.Format(
@"The client is running at port : {0}
The local end point is : {1} 
Wait for a connection....", 
                             this.port, this.listener.LocalEndpoint);
        }

        public void Stop()
        {
            //this.stream.Close();
            this.client.Close();
            this.listener.Stop();
        }

        public bool IsConnected()
        {
            return this.client.Connected;
        }

        public async void AcceptListenerAsync()
        {
            this.client = await Task.Run(() => listener.AcceptTcpClientAsync());

            this.stream = this.client.GetStream();
        }

        public void AcceptListener()
        {
            this.client = listener.AcceptTcpClient();
            
            //this.client.Client = sck;
            this.stream = this.client.GetStream();
        }

        public void SendPacket(string line)
        {
            try
            {
                System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
                byte[] msg = new byte[1500];
                //msg = enc.GetBytes(line);
                msg = System.Text.Encoding.ASCII.GetBytes(line);
                
                sck.Send(msg);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void Write(string line)
        {
            this.inString = line;
            this.inByte = new byte[256];
            this.inByte = System.Text.Encoding.ASCII.GetBytes(this.inString);
            this.stream.Write(this.inByte, 0, this.inByte.Length);
        }

        public async void WriteAsync(string line)
        {
            this.inString = line;
            this.inByte = new byte[256];
            this.inByte = System.Text.Encoding.ASCII.GetBytes(this.inString);
            await this.stream.WriteAsync(this.inByte, 0, this.inByte.Length);        
        }

        public void BeginSend(string line)
        {
            //try
            //{
                this.inString = line;
                this.inByte = new byte[System.Text.Encoding.ASCII.GetBytes(this.inString).Length];
                this.inByte = System.Text.Encoding.ASCII.GetBytes(this.inString);
                client.Client.BeginSend(this.inByte, 0, this.inByte.Length, SocketFlags.None, EndSend, null);

            //}
            //catch
            //{
            //}
        }

        private void EndSend(IAsyncResult ar)
        {
            try { client.Client.EndSend(ar); }
            catch { }
        }
    }
}
