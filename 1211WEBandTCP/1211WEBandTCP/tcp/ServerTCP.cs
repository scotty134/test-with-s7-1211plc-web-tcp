﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace _1211WEBandTCP.tcp
{
    public class ServerTCP
    {
        private Int16 port;
        private IPAddress ipAdrs;
        private TcpListener listener;
        private TcpClient client; 
        private NetworkStream stream;

        public string message { get; set; }

        public byte[] inByte { get; set; }
        public string inString { get; set; }

        public ServerTCP(Int16 p, IPAddress ip)
        {
            this.ipAdrs = ip;
            this.port = p;            
            this.listener = new TcpListener(this.ipAdrs, this.port);
            this.client = new TcpClient();
        }

        public void Start()
        {
            this.listener.Start();
            this.message = String.Empty;
            this.message = String.Format(
@"The server is running at port : {0}
The local end point is : {1} 
Wait for a connection....", 
                          this.port, this.listener.LocalEndpoint);
        }

        public void Stop()
        {
            this.client.Close();
            this.listener.Stop();
        }

        public bool IsConnected()
        {
            return this.client.Connected;
        }

        public async void AcceptListenerAsync()
        {
            this.client = await Task.Run(() => listener.AcceptTcpClientAsync());

            this.stream = this.client.GetStream();
        }

        public void AcceptListener()
        {
            this.client = listener.AcceptTcpClient();

            this.stream = this.client.GetStream();
        }

        public bool Read(out byte[] bytes, out string result)
        {
            int count;
            if(this.stream.CanRead)
            {
                this.inByte = new byte[256];
                count = this.stream.Read(this.inByte, 0, this.inByte.Length);
                this.inString = System.Text.Encoding.ASCII.GetString(this.inByte, 0, count);

                bytes = this.inByte;
                result = this.inString;
                return true;
            }
            else
            {
                this.inByte = null;
                this.inString = String.Empty;

                bytes = this.inByte;
                result = this.inString;
                return false;
            }
        }

        public async Task<bool> ReadAsync()
        {
            int count;
            this.stream = this.client.GetStream();
            if (stream.CanRead)
            {
                this.inByte = new byte[256];
                count = await stream.ReadAsync(this.inByte, 0, this.inByte.Length);
                this.inString = System.Text.Encoding.ASCII.GetString(this.inByte, 2, count);
                return true;
            }
            else
            {
                this.inByte = null;
                this.inString = String.Empty;
                return false;
            }
        }

    }
}
