﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace _1211WEBandTCP.tcp
{
    public class LanTcpC
    {
        private Int16 port;
        private IPAddress ipAdrs;
        private TcpListener listener;
        private TcpClient client; 
        private NetworkStream stream;

        public string message { get; set; }

        public byte[] outByte { get; set; }
        public string outString { get; set; }
        public byte[] inByte { get; set; }
        public string inString { get; set; }

        public LanTcpC(Int16 p, IPAddress ip)
        {
            this.ipAdrs = ip;
            this.port = p;            
            this.listener = new TcpListener(this.ipAdrs, this.port);
            this.client = new TcpClient();
        }

        public void Start()
        {
            this.listener.Start();
            this.message = String.Empty;
            this.message = String.Format(
@"The server is running at port : {0}
The local end point is : {1} 
Wait for a connection....", 
                             this.port, this.listener.LocalEndpoint);
        }

        public void Stop()
        {
            //this.stream.Close();
            this.client.Close();
            this.listener.Stop();
        }

        public bool IsConnected()
        {
            return this.client.Connected;
        }

        public async void AcceptListenerAsync()
        {
            this.client = await Task.Run(() => listener.AcceptTcpClientAsync());

            this.stream = this.client.GetStream();
        }

        public void AcceptListener()
        {
            this.client = listener.AcceptTcpClient();

            this.stream = this.client.GetStream();
        }

        public bool Read(out byte[] bytes, out string result)
        {
            int count;
            if(stream.CanRead)
            {
                bytes = new byte[256];
                count = this.stream.Read(bytes, 0, bytes.Length);
                result = System.Text.Encoding.ASCII.GetString(bytes, 0, count);
                return true;
            }
            else
            {
                bytes = null;
                result = String.Empty;
                return false;
            }
        }

        public async Task<bool> ReadAsync()
        {
            int count;
            this.stream = this.client.GetStream();
            if (stream.CanRead)
            {
                this.outByte = new byte[256];
                count = await stream.ReadAsync(this.outByte, 0, this.outByte.Length);
                this.outString = System.Text.Encoding.ASCII.GetString(this.outByte, 2, count);
                return true;
            }
            else
            {
                this.outByte = null;
                this.outString = String.Empty;
                return false;
            }
        }

        public void Write(string line)
        {
            this.inString = line;
            this.inByte = new byte[256];
            this.inByte = System.Text.Encoding.ASCII.GetBytes(this.inString);
            this.stream.Write(this.inByte, 0, this.inByte.Length);
        }

        public async void WriteAsync(string line)
        {
            this.inString = line;
            this.inByte = new byte[256];
            this.inByte = System.Text.Encoding.ASCII.GetBytes(this.inString);
            await this.stream.WriteAsync(this.inByte, 0, this.inByte.Length);        
        }

        public void BeginSend(string line)
        {

            //try
            //{
                this.inString = line;
                this.inByte = new byte[256];
                this.inByte = System.Text.Encoding.ASCII.GetBytes(this.inString);
                client.Client.BeginSend(this.inByte, 0, this.inByte.Length, SocketFlags.None, EndSend, null);

            //}
            //catch
            //{
            //}
        }

        private void EndSend(IAsyncResult ar)
        {
            try { client.Client.EndSend(ar); }
            catch { }
        }
    }
}
