﻿namespace _1211WEBandTCP
{
    partial class TcpCommFm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtMyIP = new System.Windows.Forms.TextBox();
            this.txtMyPort = new System.Windows.Forms.TextBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnDisconnect = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusWrite = new System.Windows.Forms.ToolStripStatusLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSendBytes = new System.Windows.Forms.TextBox();
            this.txtRecieveBytes = new System.Windows.Forms.TextBox();
            this.txtSendText = new System.Windows.Forms.TextBox();
            this.numSend = new System.Windows.Forms.NumericUpDown();
            this.numRcv = new System.Windows.Forms.NumericUpDown();
            this.txtRecieveText = new System.Windows.Forms.TextBox();
            this.timerPLC = new System.Windows.Forms.Timer(this.components);
            this.txtBuffer = new System.Windows.Forms.TextBox();
            this.txtTimer = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtClientPort = new System.Windows.Forms.TextBox();
            this.txtClientIP = new System.Windows.Forms.TextBox();
            this.btnWriteDisconn = new System.Windows.Forms.Button();
            this.btnWriteConn = new System.Windows.Forms.Button();
            this.btnSend = new System.Windows.Forms.Button();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRcv)).BeginInit();
            this.SuspendLayout();
            // 
            // txtMyIP
            // 
            this.txtMyIP.Location = new System.Drawing.Point(13, 13);
            this.txtMyIP.Name = "txtMyIP";
            this.txtMyIP.Size = new System.Drawing.Size(86, 20);
            this.txtMyIP.TabIndex = 0;
            this.txtMyIP.Text = "172.20.5.52";
            this.txtMyIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtMyPort
            // 
            this.txtMyPort.Location = new System.Drawing.Point(106, 13);
            this.txtMyPort.Name = "txtMyPort";
            this.txtMyPort.Size = new System.Drawing.Size(41, 20);
            this.txtMyPort.TabIndex = 1;
            this.txtMyPort.Text = "8000";
            this.txtMyPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtMyPort.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPort_KeyPress);
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(153, 11);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 2;
            this.btnConnect.Text = "connection";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Location = new System.Drawing.Point(234, 11);
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(75, 23);
            this.btnDisconnect.TabIndex = 3;
            this.btnDisconnect.Text = "Disconnect";
            this.btnDisconnect.UseVisualStyleBackColor = true;
            this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel,
            this.statusWrite});
            this.statusStrip1.Location = new System.Drawing.Point(0, 378);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(579, 24);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(20, 19);
            this.StatusLabel.Text = "   ";
            // 
            // statusWrite
            // 
            this.statusWrite.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.statusWrite.Name = "statusWrite";
            this.statusWrite.Size = new System.Drawing.Size(20, 19);
            this.statusWrite.Text = "   ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(290, 199);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "recieve :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(290, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "send :";
            // 
            // txtSendBytes
            // 
            this.txtSendBytes.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtSendBytes.Location = new System.Drawing.Point(293, 136);
            this.txtSendBytes.Multiline = true;
            this.txtSendBytes.Name = "txtSendBytes";
            this.txtSendBytes.ReadOnly = true;
            this.txtSendBytes.Size = new System.Drawing.Size(274, 60);
            this.txtSendBytes.TabIndex = 8;
            // 
            // txtRecieveBytes
            // 
            this.txtRecieveBytes.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtRecieveBytes.Location = new System.Drawing.Point(293, 215);
            this.txtRecieveBytes.Multiline = true;
            this.txtRecieveBytes.Name = "txtRecieveBytes";
            this.txtRecieveBytes.ReadOnly = true;
            this.txtRecieveBytes.Size = new System.Drawing.Size(274, 98);
            this.txtRecieveBytes.TabIndex = 9;
            // 
            // txtSendText
            // 
            this.txtSendText.Location = new System.Drawing.Point(293, 91);
            this.txtSendText.Multiline = true;
            this.txtSendText.Name = "txtSendText";
            this.txtSendText.Size = new System.Drawing.Size(204, 35);
            this.txtSendText.TabIndex = 10;
            // 
            // numSend
            // 
            this.numSend.Location = new System.Drawing.Point(503, 91);
            this.numSend.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numSend.Name = "numSend";
            this.numSend.Size = new System.Drawing.Size(64, 20);
            this.numSend.TabIndex = 11;
            // 
            // numRcv
            // 
            this.numRcv.Location = new System.Drawing.Point(501, 320);
            this.numRcv.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numRcv.Name = "numRcv";
            this.numRcv.Size = new System.Drawing.Size(66, 20);
            this.numRcv.TabIndex = 13;
            // 
            // txtRecieveText
            // 
            this.txtRecieveText.Location = new System.Drawing.Point(293, 319);
            this.txtRecieveText.Multiline = true;
            this.txtRecieveText.Name = "txtRecieveText";
            this.txtRecieveText.Size = new System.Drawing.Size(202, 46);
            this.txtRecieveText.TabIndex = 12;
            // 
            // timerPLC
            // 
            this.timerPLC.Tick += new System.EventHandler(this.timerPLC_Tick);
            // 
            // txtBuffer
            // 
            this.txtBuffer.Location = new System.Drawing.Point(13, 65);
            this.txtBuffer.Multiline = true;
            this.txtBuffer.Name = "txtBuffer";
            this.txtBuffer.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtBuffer.Size = new System.Drawing.Size(271, 300);
            this.txtBuffer.TabIndex = 14;
            // 
            // txtTimer
            // 
            this.txtTimer.Location = new System.Drawing.Point(315, 12);
            this.txtTimer.Name = "txtTimer";
            this.txtTimer.Size = new System.Drawing.Size(53, 20);
            this.txtTimer.TabIndex = 15;
            this.txtTimer.Text = "500";
            this.txtTimer.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(374, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "мс";
            // 
            // txtClientPort
            // 
            this.txtClientPort.Location = new System.Drawing.Point(106, 39);
            this.txtClientPort.Name = "txtClientPort";
            this.txtClientPort.Size = new System.Drawing.Size(41, 20);
            this.txtClientPort.TabIndex = 17;
            this.txtClientPort.Text = "8080";
            this.txtClientPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtClientIP
            // 
            this.txtClientIP.Location = new System.Drawing.Point(14, 39);
            this.txtClientIP.Name = "txtClientIP";
            this.txtClientIP.Size = new System.Drawing.Size(86, 20);
            this.txtClientIP.TabIndex = 18;
            this.txtClientIP.Text = "172.20.5.200";
            this.txtClientIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnWriteDisconn
            // 
            this.btnWriteDisconn.Location = new System.Drawing.Point(234, 37);
            this.btnWriteDisconn.Name = "btnWriteDisconn";
            this.btnWriteDisconn.Size = new System.Drawing.Size(75, 23);
            this.btnWriteDisconn.TabIndex = 20;
            this.btnWriteDisconn.Text = "Disconnect";
            this.btnWriteDisconn.UseVisualStyleBackColor = true;
            this.btnWriteDisconn.Click += new System.EventHandler(this.btnWriteDisconn_Click);
            // 
            // btnWriteConn
            // 
            this.btnWriteConn.Location = new System.Drawing.Point(153, 37);
            this.btnWriteConn.Name = "btnWriteConn";
            this.btnWriteConn.Size = new System.Drawing.Size(75, 23);
            this.btnWriteConn.TabIndex = 19;
            this.btnWriteConn.Text = "connection";
            this.btnWriteConn.UseVisualStyleBackColor = true;
            this.btnWriteConn.Click += new System.EventHandler(this.btnWriteConn_Click);
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(398, 65);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(75, 23);
            this.btnSend.TabIndex = 21;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // TcpCommFm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 402);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.btnWriteDisconn);
            this.Controls.Add(this.btnWriteConn);
            this.Controls.Add(this.txtClientIP);
            this.Controls.Add(this.txtClientPort);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtTimer);
            this.Controls.Add(this.txtBuffer);
            this.Controls.Add(this.numRcv);
            this.Controls.Add(this.txtRecieveText);
            this.Controls.Add(this.numSend);
            this.Controls.Add(this.txtSendText);
            this.Controls.Add(this.txtRecieveBytes);
            this.Controls.Add(this.txtSendBytes);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnDisconnect);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.txtMyPort);
            this.Controls.Add(this.txtMyIP);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TcpCommFm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "TcpCommFm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TcpCommFm_FormClosing);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRcv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtMyIP;
        private System.Windows.Forms.TextBox txtMyPort;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnDisconnect;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSendBytes;
        private System.Windows.Forms.TextBox txtRecieveBytes;
        private System.Windows.Forms.TextBox txtSendText;
        private System.Windows.Forms.NumericUpDown numSend;
        private System.Windows.Forms.NumericUpDown numRcv;
        private System.Windows.Forms.TextBox txtRecieveText;
        private System.Windows.Forms.Timer timerPLC;
        private System.Windows.Forms.TextBox txtBuffer;
        private System.Windows.Forms.TextBox txtTimer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtClientPort;
        private System.Windows.Forms.TextBox txtClientIP;
        private System.Windows.Forms.Button btnWriteDisconn;
        private System.Windows.Forms.Button btnWriteConn;
        private System.Windows.Forms.ToolStripStatusLabel statusWrite;
        private System.Windows.Forms.Button btnSend;
    }
}